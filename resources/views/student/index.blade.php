@extends('master')

@section('title', 'Display student page')

@section('content')
<div class="col-md-12">
    <h1 class="text-center mb-5">Student List</h1>
    <br>
    @if($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{$message}}</p>
    </div>
    @endif
    <div align="left">
        <a href="{{ route('student.create') }}" class="btn btn-primary">Add Student</a>
        
        <br><br>

    </div>

    <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Student name</th>
            <th scope="col">Student Email</th>
            <th scope="col">Student course</th>
            <th scope="col">Student address</th>

            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
          @foreach ($students as $student)
            <tr>
                <td>{{ $student['id'] }}</td>
                <td>{{ $student['name'] }}</td>
                <td>{{ $student['email'] }}</td>
                <td>{{ $student['course'] }}</td>
                <td>{{ $student['address'] }}</td>

                <td> <a href="{{ action('StudentController@edit', $student['id']) }}" class="btn btn-warning" >Edit</a> </td>
                <td>
                    <form method="post" class="delete_form" action="{{ action('StudentController@destroy', $student['id']) }}">
                        {{csrf_field()}}
                    <input type="hidden" name="_method" value="DELETE" />
                    <button type="submit" class="btn btn-danger">Delete</button>

                    </form>
                </td>
              </tr>
          @endforeach


        </thead>

      </table>
</div>
<script>
    $(document).ready(function(){
     $('.delete_form').on('submit', function(){
      if(confirm("Are you sure you want to delete it?"))
      {
       return true;
      }
      else
      {
       return false;
      }
     });
    });
    </script>

@endsection
