@extends('master')

@section('title', 'Add student page')

@section('content')
<div class="col-md-8">
<h1>ADD STUDENT</h1>
<br>

@if(count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ( $errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(\Session::has('success'))
    <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
    </div>
@endif

<form method="post" action="{{ url('student') }}">
    {{ csrf_field() }}
  <div class="form-group">
    <label>Student Name</label>
    <input type="text" name="name" class="form-control">
  </div>
  <div class="form-group">
    <label >Student Email</label>
    <input type="text" name="email"  class="form-control">
  </div>


  <div class="form-group">
    <label >Student Course</label>
    <input type="text" name="course"  class="form-control">
  </div>
  <div class="form-group">
    <label >Student address</label>
    <input type="text" name="address" class="form-control">
  </div>
  <div class="form-group">
    <input type="submit" class="btn btn-primary" />
</form>

</div>
@endsection
