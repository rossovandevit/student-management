<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['name','email', 'course', 'address'];

    // public function courses(){
    //     return $this->belongsToMany('App\Course');
    // }
}
