<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function main()
    {
        return view('main');
    }
    public function addStudent()
    {
        return view('addStudent');
    }
    public function displayStudent()
    {
        return view('displayStudent');
    }
    public function updateStudent()
    {
        return view('updateStudent');
    }

}
