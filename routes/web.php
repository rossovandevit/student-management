<?php

use App\Course;
use App\Student;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/main', 'HomeController@main');



//CRUD
Route::resource('student', 'StudentController');




// //test
// Route::get('/createmtm', function () {
//     $student = Student::find(2);
//     $course = new Course(['name'=> 'ABC']);
//     $student->courses()->save($course);
//     echo 'data is inserted';
// });

// Route::get('readmtm', function () {
//     $student = Student::findOrFail(1);
//     foreach($student->courses as $course){
//         echo $student->name ." ". $student->email." ".$course->name. "<br>";
//     }
// });

